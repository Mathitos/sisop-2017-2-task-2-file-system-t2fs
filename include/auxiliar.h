#ifndef AUXILIAR_H
#define AUXILIAR_H

#include "t2fs.h"
#include "apidisk.h"

#define boolean char
#define true 1
#define false 0

#define	MAX_FILE_NAME_SIZE	55
#define MAX_FILES_OPENED 20S
#define BLOCK_SIZE 256 //bytes


typedef struct {

	boolean opened;
	struct t2fs_record record;
	DWORD current_pointer;

}OpenedFile;
typedef struct{

	boolean opened;
	struct t2fs_record record;
	DWORD current_pointer;

}OpenedDirectory;


struct t2fs_superbloco *superblock;
int filesOpened;
int directoriesOpened;
DWORD currentDirectoryBlock;
DWORD rootBlock;


#endif
