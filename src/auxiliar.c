#include "../include/apidisk.h"
#include "../include/t2fs.h"
#include "../include/auxiliar.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>



int readSuperBlock(){
	superblock = (struct t2fs_superbloco*) malloc (SECTOR_SIZE);
	if (read_sector(0, (char*) superblock) != 0){
		printf("Erro ao tentar ler o superbloco");
		return -1;
	}

	rootBlock = (superblock->pFATSectorStart);
	currentDirectoryBlock = rootBlock;
	filesOpened = 0;
	directoriesOpened = 1;
}

int readBlock(DWORD block, char* buffer){
	if (superblock == NULL)
	readSuperblock();

	for (int i = 0; i < BLOCK_SIZE; i++)
	{
		if (read_sector ((block*BLOCK_SIZE)+i, &buffer[SECTOR_SIZE*i]) != 0){
			printf("Erro ao tentar ler o bloco %d\n",block);
			return -1;
		}
	}
	return 0;
}

int writeBlock(DWORD block, char* buffer){
	if (superblock == NULL)
		readSuperblock();

	for (int i = 0; i < BLOCK_SIZE; i++)
	{
		if (write_sector ((block*BLOCK_SIZE)+i, &buffer[SECTOR_SIZE*i]) != 0){
			printf("Erro ao tentar escrever no bloco %d\n", block);
			return -1;
		}
	}
	return 0;
}

int validateFilename(char *filename){
	for (int i = 0; i < MAX_FILE_NAME_SIZE; i++)
	{
		if (filename[i] == '\0')
		{
			if (i == 0){
				printf("Nome do arquivo não pode ser vazio\n");
				return -1;
			}
			else
				return 0;
		}

		if (filename[i] < 0x21 || filename[i] > 0x7A){
			printf("Nome do arquivo não pode conter caracteres inválidos\n");
			return -1;
		}
		if (filename[i] == '/'){
			printf("Nome do arquivo não pode conter '/'\n");
			return -1;
		}

		printf("Nome do arquivo não contem caractere de encerramento de string\n");
		return -1;
}
